import express from 'express';
import container from '../container';
import { adapt } from '../drivers/http/express-router-adapter';

import CreateDepartmentController from './controllers/department/createDepartment';
import GetAllDepartmentsController from './controllers/department/getAllDepartaments';
import DeleteDepartmentByIdController from './controllers/department/deleteDepartmentById';
import UpdateDepartmentController from './controllers/department/updateDepartmentById';

const router = express.Router();

router.post('/department', adapt(container.resolve(CreateDepartmentController)));
router.get('/department', adapt(container.resolve(GetAllDepartmentsController)));
router.delete('/department/:id', adapt(container.resolve(DeleteDepartmentByIdController)));
router.put('/department/:id', adapt(container.resolve(UpdateDepartmentController)));

export default router;
