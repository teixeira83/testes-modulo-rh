import { injectable } from 'tsyringe';

import Department from '../../../interfaces/entities/department/department';
import Controller from '../../../interfaces/http/controller';
import { HttpResponse } from '../../../interfaces/http/http';

import GetAllDepartmentsUseCase from '../../../use-cases/department/getAllDepartments';

@injectable()
class GetAllDepartmentsController implements Controller {
  constructor(
    private getAllDepartmentsUseCase: GetAllDepartmentsUseCase,
  ) { }
  async handle(): Promise<HttpResponse> {
    try {
      const allDepartments:Department[] = await this.getAllDepartmentsUseCase.execute();

      return {
        body: {
          products: allDepartments,
        },
        status: 200,
      } as HttpResponse;
    } catch (error: any) {
      return {
        body: 'Erro at GetAllDepartmentsController',
        status: 400,
      } as HttpResponse;
    }
  }
}

export default GetAllDepartmentsController;
