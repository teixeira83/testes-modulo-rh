import { injectable } from 'tsyringe';

import Controller from '../../../interfaces/http/controller';

import { HttpRequest, HttpResponse } from '../../../interfaces/http/http';

import DeleteDepartmentByIdUseCase from '../../../use-cases/department/deleteDepartmentById';

import { ValidationError } from '../../../errors';

@injectable()
class DeleteDepartmentByIdController implements Controller {
  constructor(
    private deleteDepartmentByIdUseCase: DeleteDepartmentByIdUseCase,
  ) { }
  async handle(httpRequest: HttpRequest): Promise<HttpResponse> {
    try {
      const { params } = httpRequest;
      
      const departmentWasSucessfulRemoved:boolean = await this.deleteDepartmentByIdUseCase.execute(Number(params?.id));
      
      console.log('params')
      console.log(params?.id)
      console.log(departmentWasSucessfulRemoved)
      console.log('params')

      if (!departmentWasSucessfulRemoved) {
        console.log('DENTRO')
        throw new ValidationError('Cannot delete this department.', 404);
      }

      return {
        body: {
          departmentWasRemoved: departmentWasSucessfulRemoved,
        },
        status: 200,
      };
    } catch (error: any) {
      return {
        body: error,
        status: error.status,
      };
    }
  }
}

export default DeleteDepartmentByIdController;
