import { injectable } from 'tsyringe';
import isEmpty from 'is-empty';

import Controller from '../../../interfaces/http/controller';
import { HttpRequest, HttpResponse } from '../../../interfaces/http/http';

import CreateDepartmentUseCase from '../../../use-cases/department/createDepartment';

import { ValidationError } from '../../../errors';

@injectable()
class CreateDepartmentController implements Controller {
  constructor(
    private createDepartmentUseCase: CreateDepartmentUseCase,
  ) { }

  async handle(httpRequest: HttpRequest): Promise<HttpResponse> {
    try {
      const { body }:HttpRequest = httpRequest;

      if (!(typeof body === 'object') || isEmpty(body)) {
        throw new ValidationError('bodyShouldNotBeEmpty', 400);
      }

      if (!('name' in body)) {
        throw new ValidationError('Missing paramns, check API docs', 400);
      }

      await this.createDepartmentUseCase.execute(body);

      return {
        body: {
          message: 'Department successful created',
        },
        status: 200,
      } as HttpResponse;
    } catch (error) {
      return {
        body: error,
        status: 400,
      }
    }
  }
}

export default CreateDepartmentController;
