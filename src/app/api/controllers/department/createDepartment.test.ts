import { mock } from 'jest-mock-extended';
import CreateDepartmentUseCase from '../../../use-cases/department/createDepartment';
import CreateDepartmentController from './createDepartment';
import { HttpRequest } from '../../../interfaces/http/http';

describe('Test controller createDepartment', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  test('Should return status 400 when send no body', async () => {
    const mockedUsecase = mock<CreateDepartmentUseCase>();
    const Sut = new CreateDepartmentController(mockedUsecase);

    const mockedRequest : HttpRequest = {
      body: {}
    };

    const result = await Sut.handle(mockedRequest);
    expect(result.status).toBe(400);
  });

  test('Should return status 200 when send correct body', async () => {
    const mockedUsecase = mock<CreateDepartmentUseCase>();
    const Sut = new CreateDepartmentController(mockedUsecase);
    
    const mockedRequest : HttpRequest = {
      body: {
        name: 'DESENVOLVIMENTO'
      },
    };

    const result = await Sut.handle(mockedRequest);
    expect(result.status).toBe(200);
  });

  test('Should return status 400 when send body without correctly params', async () => {
    const mockedUsecase = mock<CreateDepartmentUseCase>();
    const Sut = new CreateDepartmentController(mockedUsecase);
    
    const mockedRequest : HttpRequest = {
      body: {
          username: 'Rafael'
      }
    };
  
    const result = await Sut.handle(mockedRequest);
    expect(result.status).toBe(400);
  })

});
