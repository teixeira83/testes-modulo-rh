import { mock } from 'jest-mock-extended';
import DeleteDepartmentByIdUseCase from '../../../use-cases/department/deleteDepartmentById';
import DeleteDepartmentByIdController from './deleteDepartmentById';
import { HttpRequest } from '../../../interfaces/http/http';

describe('Test controller DeleteDepartmentById', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    test('Should return status 404 when send an id that dont exist in database', async () => {
        const mockedUsecase = mock<DeleteDepartmentByIdUseCase>();
        const Sut = new DeleteDepartmentByIdController(mockedUsecase);

        const mockedRequest : HttpRequest = {
            params: {
                id: '99'
            }
        };

        const result = await Sut.handle(mockedRequest);
        expect(result.status).toBe(404);
    });

    // test('Should return status 200 when send an exist id', async () => {
    //     const mockedUsecase = mock<DeleteDepartmentByIdUseCase>();
    //     const Sut = new DeleteDepartmentByIdController(mockedUsecase);

    //     const mockedRequest : HttpRequest = {
    //         params: {
    //             id: '10'
    //         }
    //     };

    //     const result = await Sut.handle(mockedRequest);
    //     console.log(result)
    //     expect(result.status).toBe(200);
    // });
});
