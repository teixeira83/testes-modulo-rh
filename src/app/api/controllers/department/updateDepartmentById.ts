import { injectable } from 'tsyringe';
import isEmpty from 'is-empty';

import Controller from '../../../interfaces/http/controller';
import { HttpRequest, HttpResponse } from '../../../interfaces/http/http';

import UpdateDepartmentByIdUseCase from '../../../use-cases/department/updateDepartmentById';

import { ValidationError } from '../../../errors';

@injectable()
class UpdateDepartmentController implements Controller {
  constructor(
    private updateDepartmentById: UpdateDepartmentByIdUseCase,
  ) { }

  async handle(httpRequest: HttpRequest): Promise<HttpResponse> {
    try {
      const { body, params }:HttpRequest = httpRequest;

      if (!(typeof body === 'object') || isEmpty(body)) {
        throw new ValidationError('bodyShouldNotBeEmpty', 400);
      }

      if (!('name' in body)) {
        throw new ValidationError('Missing paramns, check API docs', 400);
      }

      await this.updateDepartmentById.execute(body, params?.id);

      return {
        body: {
          message: 'Department successful updated',
        },
        status: 200,
      } as HttpResponse;
    } catch (error) {
      return {
        body: error,
        status: 400,
      }
    }
  }
}

export default UpdateDepartmentController;
