import supertest from 'supertest';
import startExpressServer from '../drivers/http/server';

describe('Test department endpoints', () => {
  
  test('Should not return 404', async () => {
    const apiMocked = await supertest(startExpressServer());
    const result = await apiMocked.get('/department');
    expect(result.statusCode).not.toBe(404);
  });
});
