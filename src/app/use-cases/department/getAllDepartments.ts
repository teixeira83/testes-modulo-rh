import { inject, injectable } from 'tsyringe';
import Department from '../../interfaces/entities/department/department';
import UseCase from '../../interfaces/use-case';
import DepartmentRepository from 'app/interfaces/entities/department/department-repository';

@injectable()
class GetAllDepartmentsUseCase implements UseCase {
  constructor(
    @inject('DepartmentRepository') private departmentRepository: DepartmentRepository,
  ) {}

  async execute(): Promise<Department[]> {
    const allDepartments: Department[] = await this.departmentRepository.getAllDepartments();
    return allDepartments;
  }
}

export default GetAllDepartmentsUseCase;
