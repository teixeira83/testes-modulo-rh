import { inject, injectable } from 'tsyringe';
import UseCase from '../../interfaces/use-case';
import DepartmentRepository from '../../interfaces/entities/department/department-repository';

@injectable()
class CreateDepartmentUseCase implements UseCase {
  constructor(
    @inject('DepartmentRepository') private departmentRepository: DepartmentRepository,
  ) {}

  async execute(input: any): Promise<boolean> {
    const departmentWasCreated = await this.departmentRepository.createDepartment(input);
    return departmentWasCreated;
  }
}

export default CreateDepartmentUseCase;
