import { inject, injectable } from 'tsyringe';
import UseCase from '../../interfaces/use-case';
import DepartmentRepository from '../../interfaces/entities/department/department-repository';

@injectable()
class UpdateDepartmentByIdUseCase implements UseCase {
  constructor(
    @inject('DepartmentRepository') private departmentRepository: DepartmentRepository,
  ) {}

  async execute(input: any, params: unknown): Promise<number> {
    const idOfUpdatedDepartment: number = await this
      .departmentRepository.updateDepartmentById(input, params);
    return idOfUpdatedDepartment;
  }
}

export default UpdateDepartmentByIdUseCase;
