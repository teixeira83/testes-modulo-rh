import { inject, injectable } from 'tsyringe';
import UseCase from '../../interfaces/use-case';
import DepartmentRepository from '../../interfaces/entities/department/department-repository';

@injectable()
class DeleteDepartmentByIdUseCase implements UseCase {
  constructor(
    @inject('DepartmentRepository') private departmentRepository: DepartmentRepository,
  ) {}

  async execute(input: any): Promise<boolean> {
    const departmentWasSucessfulRemoved = await this.departmentRepository.deleteDepartmentById(input);
    return departmentWasSucessfulRemoved;
  }
}

export default DeleteDepartmentByIdUseCase;
