import Department from './department';

export default interface DepartmentRepository {
  getAllDepartments(): Promise<Department[]>;
  createDepartment(departmentToBeAdded: Department): Promise<boolean>;
  updateDepartmentById(departmentBeModified: Department, deparmentId: unknown): Promise<number>;
  deleteDepartmentById(departmentIdToBeDeleted: string): Promise<boolean>;
}
