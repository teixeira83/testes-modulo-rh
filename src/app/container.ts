import { container } from 'tsyringe';

import DepartmentMySqlDBRepository from './drivers/mysql/department/department-mysql.repository';

container.register('DepartmentRepository', {
  useClass: DepartmentMySqlDBRepository,
});

export default container;
