import { Entity, PrimaryColumn, Column } from 'typeorm';
  
@Entity()
export default class Department {

    @PrimaryColumn()
    id!: number;

    @Column()
    name!: string;
}