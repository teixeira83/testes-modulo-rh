import { getConnectionManager, ConnectionManager } from 'typeorm';

import DepartmentRepository from '../../../interfaces/entities/department/department-repository';
import Department from '../../../interfaces/entities/department/department';

import { EnitityNotFound } from '../../../errors';

import DepartmentEntity from './department-entity';

class DepartmentMySqlDBRepository implements DepartmentRepository {
  private connectionManager: ConnectionManager;
  constructor() {
    this.connectionManager = getConnectionManager();
  }

  async createDepartment(departmentToBeAdded: Department): Promise<boolean> {
    try {
      const connection = this.connectionManager.get();
      await connection
        .createQueryBuilder()
        .insert()
        .into(DepartmentEntity, ['name'])
        .values([
          {
            name: departmentToBeAdded.name,
          },
        ])
        .execute();
      return true;
    } catch (error: any) {
    //   if (error.code === 'ER_DUP_ENTRY') {
    //     throw new DuplicatedKeyError(`Code ${departmentToBeAdded.code} duplicated`, 409);
    //   }
      console.log(error)
      return false;
    }
  }

  async getAllDepartments(): Promise<Department[]> {
    const connection = this.connectionManager.get();
    const allDepartments = await connection
      .createQueryBuilder()
      .select('*')
      .from(DepartmentEntity, 'departament')
      .getRawMany();
    return allDepartments;
  }

  async deleteDepartmentById(departmentIdToBeDeleted: string): Promise<boolean> {
    const connection = this.connectionManager.get();
    const departmentToBeRemoved = await connection
      .createQueryBuilder()
      .delete()
      .from(DepartmentEntity)
      .where('id = :id', { id: departmentIdToBeDeleted })
      .execute()

      if (departmentToBeRemoved.affected === 0) {
        // throw new EnitityNotFound(`Code ${productIdToBeDeleated} didnt found`, 404);
        console.log('Department didnt found')
        return false;
      }
      
      return true;
  }

  async updateDepartmentById(departmentToBeModified: Department, deparmentId: number): Promise<number> {
    const connection = this.connectionManager.get();
    const departamentToBeUpdated = await connection
      .createQueryBuilder()
      .update(DepartmentEntity)
      .set({
        name: departmentToBeModified.name,
      })
      .where('department.id = :id', { id: deparmentId })
      .execute();
    if (departamentToBeUpdated.affected === 0) {
      throw new EnitityNotFound(`Code ${deparmentId} didnt found`, 404);
    }

    return deparmentId
  }
}

export default DepartmentMySqlDBRepository;
