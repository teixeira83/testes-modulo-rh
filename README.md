# automated tests class 

This API project was done for a automated tests class. We were tasked with developing an API in nodejs, using typescript and jest for the tests. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine, for development and testing purposes. 

### Prerequisites

You must install node.js, jest and mysql-server.

```
I strongly recommend downloading the LTS versions from:
https://nodejs.org/en/ 
https://jestjs.io/pt-BR/docs/getting-started
https://dev.mysql.com/downloads/mysql/

```

### Installing

First, you'll clone the repository from github. 
Then, open the folder you just cloned and install the dependencies using npm or yarn as you wish

```
git clone https://github.com/LeoFC97/tdd-ts-mglu-study
cd testes-modulo-rh
yarn
```

### Startup database
To startup the database two steps are necessary: 

Create .env file at source folder and fill with your database data like .env-example that already exists in the project:
```
MYSQL_HOST=localhost
MYSQL_USERNAME=root
MYSQL_PASSWORD=root
MYSQL_DATABASE=testesrh
```
You also need to create the database tables at your local machine. 
For now only crud of the "department" table is implemented


## Request & Response Examples

### API Resources

### Department
  - [GET /department](#get-department)
  - [POST /department](#post-department)
  - [DEL /department](#del-department)
  - [PUT /department](#put-department)

### GET /department

Example: http://localhost:3000/department

Response body:
```
{
  "department": [
    {
      "id": "1",
      "name": "PROJETOS"
    },
    {
      "id": "2",
      "name": "COMPRAS"
    }
  ]
}
```
### POST /department

Example: http://127.0.0.1:3000/clientes/1

Response body:
```{
  "client": {
    "id": "1",
    "name": "PROJETOS"
  }
}
```

### DEL /department

Example: http://localhost:3000/department/:id

Response body:
```
{
  "departmentWasRemoved": true
}
```

### PUT /department

Example: http://127.0.0.1:3000/department/:id

Request body:
```
{
	"name":"CENTRO DE CUSTOS",
}
```
Response body:

```
{
  "departmentIdUpdated": "2"
}
```

## Running the Unity tests

There are Unity Tests on the project in order to ensure that it runs cohesively, and they can be found at their respective folders.
The tests cover subjects such as string sanitazing and sorting methods. The tests in place are there to make sure the code isn't broken while the API was being developed - it is essential for present and future maintenance.

```
npm run test
or
yarn test
```

### Style guide

The standard eslint rules were applied, and the full list can be found at https://eslint.org/docs/rules/
If you want to configure your own eslint rules, the file can be found at [.eslintrc.json](./.eslintrc.json)

```
{
  "env": {
    "node": true,
    "es2020": true
  },
  "ignorePatterns": ["dist", "node_modules", "**/vendor/*.js"],
  "plugins": [
    "@typescript-eslint",
    "promise"
  ],
  "extends": [
    "airbnb-typescript/base",
    "plugin:@typescript-eslint/recommended",
    "plugin:promise/recommended"
  ],
  "globals": {
    "Atomics": "readonly",
    "SharedArrayBuffer": "readonly"
  },
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "ecmaVersion": 11,
    "sourceType": "module",
    "project": "./tsconfig.json"
  },
  "settings": {
    "import/resolver": {
      "node": {
        "moduleDirectory": [
          "node_modules",
          "src"
        ]
      },
      "typescript": {}
    }
  },
  "rules": {
    "complexity": [
      "error",
      {
        "max": 5
      }
    ],
    "lines-between-class-members": "off",
    "@typescript-eslint/lines-between-class-members": "off",
    "class-methods-use-this": "off",
    "linebreak-style": 0
  }
}
```


## Enviroment

 You must have setup your enviroment .env file at project root when deploying the API.


## Built With

* [Nodejs](https://nodejs.org/en/docs/) - JavaScript runtime built on Chrome's V8 JavaScript engine.
* [Mysql](https://dev.mysql.com/downloads/mysql/) - Used database.
* [Express](https://www.npmjs.com/package/express/) - HTTP server.
* [TypeScript](https://www.typescriptlang.org/) - Elegant way to code Javascript xD.
* [Jest](https://jestjs.io/pt-BR/docs/getting-started) - A framework to test your entire application.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE) file for details

